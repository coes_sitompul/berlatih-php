<?php 
  function tentukan_nilai($nilai) {
    if($nilai >= 85 && $nilai <= 100) {
      return "Sangat Baik <br>";
    } else if($nilai >= 70 && $nilai < 85) {
      return "Baik <br>";
    } else if($nilai >= 60 && $nilai < 70) {
      return "Cukup <br>";
    } else if($nilai > 100) {
      return "Nilai Berlebihan <br>";
    } else {
      return "Kurang <br>";
    }
  }

  echo "Soal No. 1 <br><br>";
  echo tentukan_nilai(98); //Sangat Baik
  echo tentukan_nilai(76); //Baik
  echo tentukan_nilai(67); //Cukup
  echo tentukan_nilai(43); //Kurang

?>